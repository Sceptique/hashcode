#include <algorithm>
#include "Line.hpp"
#include "Server.hpp"

bool Line::isUsedPlace(Server *s) {
  return !isEmptyPlace(s) && !isLockedPlace(s);
}

bool Line::isLockedPlace(Server *s) {
  return s == LOCKED_PLACE;
}

bool Line::isEmptyPlace(Server *s) {
  return s == EMPTY_PLACE;
}

Line::Line(int const nbplaces) : _nbplaces(nbplaces) {
  _places.reserve(nbplaces);
  std::fill_n(_places.begin(), nbplaces, EMPTY_PLACE);
}

Line::~Line() {
}

place_iterator Line::findFirstEmptyPlace() {
  return findFirstEmptyPlace(1);
}
place_iterator Line::findFirstEmptyPlace(size_t const size) {
  return std::search_n(_places.begin(), _places.end(), size, EMPTY_PLACE);
}

bool Line::addServer(Server *const s) {
  place_iterator it = findFirstEmptyPlace(s->_size);
  if (it == _places.end())
    return false;
  int n = s->_size;
  while (n--)
    (*it++) = s;
  return true;
}

bool Line::setLocked(size_t const n) {
  if (n >= _nbplaces)
    return false;
  _places[n] = LOCKED_PLACE;
  return true;
}

std::ostream& operator<<(std::ostream &s, const Line &l) {
  for (int i = 0; i < l._nbplaces; i++)
    s << (l._places[i] == LOCKED_PLACE ? 'x' : (l._places[i] == EMPTY_PLACE ? '-' : 's'));
  return s;
}
