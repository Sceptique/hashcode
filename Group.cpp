#include "Group.hpp"
#include "Server.hpp"

Group::Group() {
}

Group::~Group() {
}

double Group::power() const {
  std::vector<Server *>::const_iterator it = _servers.begin();
  double power;
  while (it++ != _servers.end())
    power += (*it)->_power;
  return power;
}

double Group::weight() const {
  if (_servers.empty())
    return 0.0;
  std::vector<Server *>::const_iterator it = _servers.begin();
  double power;
  while (it++ != _servers.end())
    power += (*it)->_power;
  return power / _servers.size();
}

void Group::addServer(Server *const s) {
  _servers.push_back(s);
}
