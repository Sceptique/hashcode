#ifndef SERVER_H_
# define SERVER_H_

# include <iostream>
# include <string>
# include <vector>
# include "Group.hpp"
# include "Line.hpp"

class Server
{
public:
  Server(int const id, int const power, int const size);
  virtual ~Server();
  Server(const Server &);
  Server &operator=(const Server &);

  double weight() const;

  int const _id;
  int const _power;
  int const _size;
  int _x;
  int _y;
  Group *_group;
  Line *_line;

protected:

};

#endif /* !SERVER_H_ */
