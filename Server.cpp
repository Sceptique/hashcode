#include "Server.hpp"

Server::Server(int const id, int const power, int const size) :
  _id(id), _size(size), _power(power)
{
}

Server::~Server() {
}

double Server::weight() const {
  return _power / _size;
}
